json.extract! animal, :id, :name, :birth, :sex, :description, :observations, :adopted, :owner, :phone, :mobile_phone, :email, :created_at, :updated_at
json.url animal_url(animal, format: :json)
