class CreateSuccessStories < ActiveRecord::Migration[5.2]
  def change
    create_table :success_stories do |t|
      t.string :pet_name
      t.string :owner
      t.string :photo
      t.text :description

      t.timestamps
    end
  end
end
